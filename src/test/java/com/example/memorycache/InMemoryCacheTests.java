package com.example.memorycache;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class InMemoryCacheTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void homeResponse() {
		String body = this.restTemplate.getForObject("/", String.class);
		assertThat(body).isEqualTo("Spring is here!");
	}

	public static void main(String[] args) throws InterruptedException {

		InMemoryCacheTests Cache = new InMemoryCacheTests();

		System.out.println("\n\n==========Test1: TestAddRemoveObjects ==========");
		Cache.TestAddRemoveObjects();
		System.out.println("\n\n==========Test2: TestExpiredCacheObjects ==========");
		Cache.TestExpiredCacheObjects();
		System.out.println("\n\n==========Test3: TestObjectsCleanupTime ==========");
		Cache.TestObjectsCleanupTime();
	}

	private void TestAddRemoveObjects() {
		InMemoryCache<String, String> cache = new InMemoryCache<String, String>(200, 500, 3);

		cache.put("firstObject", "1st object");
		cache.put("secondObject", "2nd object");
		cache.put("thirdObject", "3rd object");

		System.out.println("3 Cache Objects were applied.. cache.size(): " + cache.size());
		cache.remove("secondObject");
		System.out.println("One object was removed.. cache.size(): " + cache.size());

		cache.put("fourthObject", "4th object");
		cache.put("fifthObject", "5th object");
		System.out.println("Two objects were applied extra but reached maxItems.. cache.size(): " + cache.size());

	}

	private void TestExpiredCacheObjects() throws InterruptedException {

		InMemoryCache<String, String> cache = new InMemoryCache<String, String>(1, 1, 10);

		cache.put("firstObject", "1st object");
		cache.put("secondObject", "2nd object");
		Thread.sleep(3000);

		System.out.println("Two objects were applied but reached timeToLive. cache.size(): " + cache.size());

	}

	private void TestObjectsCleanupTime() throws InterruptedException {
		int size = 500000;

		InMemoryCache<String, String> cache = new InMemoryCache<String, String>(100, 100, 500000);

		for (int i = 0; i < size; i++) {
			String value = Integer.toString(i);
			cache.put(value, value);
		}

		Thread.sleep(200);

		long start = System.currentTimeMillis();
		cache.cleanup();
		double finish = (double) (System.currentTimeMillis() - start) / 1000.0;

		System.out.println("Cleanup times for " + size + " objects are " + finish + " s");

	}

}
